<?php

// [SECTION] Comments

/*
	There are two types of comments:
	- single-line comment denoted with two forward slashes (ctrl + /)
	- multiline comment denoted by a forward slash and asterisk (ctrl + shift + /)

*/

// [SECTION] Variables

	// Variables are defined using the dollar ($) notation before the variable name.

	$name = "John Smith";
	$email = "johnsmith@gmail.com";

// [SECTIONS] Constants
// Constants are defined using the define() function.
// Naming convention for "constant" variables should be in ALL CAPS.
// Doesn't use the $ notation before the variable. 
define('PI', 3.1416);

// Variable vs Constant
// Constants do not follow any variable scoping.
// Varaibles can be declared anywhere in the program, but they follow variable scoping rules.

// [SECTION] Echoing Values

// [SECTION] Data Types

// String
$state = 'New York';
$country = 'United States of America';

// Single quote concatenation via . sign
$address = $state .', '. $country;

// Concantenation via double quote
$address = "$state, $country";

// Integers
$age = 31;
$headcount = 26;

// Floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

// Boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;

// Arrays
// array() function is used to declare arrays.
$grades = array(98.7, 92.1, 90.2, 94.6);

// Null
$spouse = null;
$middleName = null;

// Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'United States of America'
	]
];

// [SECTION] Operators

// Assignment Operator (=)

$x = 250;
$y = 120;

$isLegalAge = true;
$isRegistered = false;

// [SECTION] Functions
// Functions are used to make reusable code.
function getFullName($firstName, $middleInitial, $lastName){
	return "$lastName, $firstName, $middleInitial";
}

// [SECTION] Selection Control Structures

// If-Elseif-Else Statement
function determineTyphoonIntensity($windSpeed){
	if($windSpeed < 30){
		return 'Not a typhoon yet.';
	}
	else if ($windSpeed <= 61){
		return 'Tropical depression detected.';
	}
	else if($windSpeed >= 62 && $windSpeed <=88){
		return 'Trophical Storm detected.';
	}
	else if($windSpeed >= 89 && $windSpeed <= 117){
		return 'Severe Trophical Storm detected.';
	}
	else{
		return 'Typhone detected';
	}
}

// Conditional (Ternary) Operator
function isUnderAge($age){
	return ($age < 18) ? true : false;
}

// Switch Statement
function determineComputerUser($computerNumber) {
    switch ($computerNumber) {
        case 1: 
            return 'Linus Torvalds';
            break;
        case 2:
            return 'Steve Jobs';
            break;
        case 3:
            return 'Sid Meier';
            break;
        case 4:
            return 'Onel de Guzman';
            break;
        case 5:
            return 'Christian Salvador';
            break;
		default:
            return $computerNumber.' is out of bounds.';
            break;
    }
}

// Try-Catch-Finally Statement

function greeting($str){
	try{
		// Attempt to execute a code.
		if(gettype($str) === "string"){
			echo $str;
		}
		else{
			//instantiate a new Exception object from PHP's pre-defined Exception class using its constructor
            //the Exception object created here will have a message "Oops!"
            //classes and objects will be discussed in further details in a future session, this is just to demonstrate triggering of 
			throw new Exception("Oops!");
		}
	}
	catch (Exception $e){
		// Catch errors within 'try', in this case the error is not a "string" data type.
	        // $e is an object we a getter function "getMessage()"
		echo $e->getMessage();
	}
	finally{
		// Continue execution of code regardless of success and failure of code execution in 'try' block.
		echo " I did it again!";
	}
}